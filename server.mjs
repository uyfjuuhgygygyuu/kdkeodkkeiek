import WebSocket from 'ws';

const wss = new WebSocket.Server({ port: 8080 });

let clients = [];

wss.on('connection', function connection(ws) {
  clients.push(ws);
  ws.isAlive = true;

  ws.on('pong', function() {
    this.isAlive = true;
  });

  ws.on('message', function incoming(message) {
    console.log('received: %s', message);
    if (message === 'online') {
      broadcast('user online');
    }
  });

  ws.on('close', function() {
    clients = clients.filter(client => client !== ws);
    broadcast('user offline');
  });
});

function broadcast(data) {
  clients.forEach(function each(client) {
    if (client.readyState === WebSocket.OPEN) {
      client.send(data);
    }
  });
}

const interval = setInterval(function ping() {
  wss.clients.forEach(function each(ws) {
    if (ws.isAlive === false) return ws.terminate();
    ws.isAlive = false;
    ws.ping();
  });
}, 30000);

wss.on('close', function close() {
  clearInterval(interval);
});
